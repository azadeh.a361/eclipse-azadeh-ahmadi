package question1;

public class SalariedEmployee implements Employee{
	private double yearlyPay;
	
	
	public  SalariedEmployee(double yearlyPay) {
		this.yearlyPay=yearlyPay;
	}
	
	public double getYearlyPay() {
		return this.yearlyPay;
	}
	

}
