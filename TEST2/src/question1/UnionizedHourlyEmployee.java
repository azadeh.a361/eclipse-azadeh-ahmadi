package question1;

public class UnionizedHourlyEmployee extends HourlyEmployee{
private double pensionContribution;

public UnionizedHourlyEmployee(double hours,double hourlyPay,double pensionContribution ) {
	super(hours,hourlyPay);
	this.pensionContribution=pensionContribution;
	
}
@Override
public double getYearlyPay() {
	double yearlyPay=gethours()*this.gethourlyPay()*52+pensionContribution;
	return yearlyPay;
}
}
