package question1;

public class PayrollManagement {

	public static void main(String[] args) {
	Employee[] allEmployee=new Employee[5];
	allEmployee[0]=new HourlyEmployee(40.00 , 20.00);
	allEmployee[1]=new SalariedEmployee(22.00);
	allEmployee[2]=new UnionizedHourlyEmployee(40,20.00,300);
	allEmployee[3]=new UnionizedHourlyEmployee(160,21.00,200);
	allEmployee[4]=new SalariedEmployee(22.00);
	System.out.print( getTotalExpenses(allEmployee));
	}
	
	
	public static double getTotalExpenses(Employee[]allEmployee) {
		double totalExpenses=0;
		for (Employee x: allEmployee) {
			
			totalExpenses=totalExpenses+x.getYearlyPay();
		}
		return totalExpenses;
	}

}
