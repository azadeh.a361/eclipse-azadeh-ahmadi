package question1;

public class HourlyEmployee implements Employee {
	private double hours;
	private double hourlyPay;
	public HourlyEmployee(double hours,double hourlyPay) {
		this.hours=hours;
		this.hourlyPay=hourlyPay;
	}
	public double gethours() {
		return this.hours;
	}
	public double gethourlyPay() {
		return this.hourlyPay;
	}
	

	public double getYearlyPay() {
		 double yearlyPay=this.hours*this.hourlyPay*52;
		return yearlyPay;
	}

}
